var cyto_containers = document.querySelectorAll('[data-cytoscape]');
var cytos = {};

for (i = 0; i < cyto_containers.length; ++i) {
  // container content to object
  var cyto_container = cyto_containers[i];
  var obj = eval('(' + cyto_container.innerHTML + ')');
  obj.container = cyto_container;

  // remove content and display graph
  cyto_container.innerHTML = '';

  // add event listener
  Reveal.addEventListener('slidechanged', function(event) {
    if (cyto_container.parentElement == event.currentSlide) {
      if (!cytos[cyto_container]) {
        var c = cytoscape(obj);
        cyto_container.style.visibility = 'visible';
        cytos[cyto_container] = c;
      }
    }
  });


}
